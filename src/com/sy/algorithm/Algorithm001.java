package com.sy.algorithm;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * 给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target 的那 两个 整数，并返回它们的数组下标。
 * 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
 * 你可以按任意顺序返回答案。
 *
 * 示例 1：
 *
 * 输入：nums = [2,7,11,15], target = 9
 * 输出：[0,1]
 * 解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
 * 示例 2：
 *
 * 输入：nums = [3,2,4], target = 6
 * 输出：[1,2]
 * 示例 3：
 *
 * 输入：nums = [3,3], target = 6
 * 输出：[0,1]
 *  
 *
 * 提示：
 *
 * 2 <= nums.length <= 10^4
 * -10^9 <= nums[i] <= 10^9
 * -10^9 <= target <= 10^9
 * 只会存在一个有效答案
 * 进阶：你可以想出一个时间复杂度小于 O(n2) 的算法吗？
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/two-sum
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * @Author zb
 * @Date 2022/3/7 16:21
 */
public class Algorithm001 {
    public static void main(String[] args){

        Solution solution = new Solution();
        int[] nums = new int[]{3,2,4};
        int target = 6;

        int[] result = solution.twoSum(nums,target);
        if(result != null && result.length > 0){
            System.out.println(result[0]+" "+result[1]);
        }
    }

    private static class Solution {
        public int[] twoSum(int[] nums, int target) {
            //最快实现 for(for)判断  O(n^2) 太简单就不写了
            //进阶
        /*Map<Integer,Integer> indexRelation = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            indexRelation.put(nums[i],i);
        }
        for (int i = 0; i < nums.length; i++) {
            int diff = target - nums[i];
            if(indexRelation.get(diff)!=null && i!=indexRelation.get(diff)){
                return new int[]{i,indexRelation.get(diff)};
            }
        }*/

            //最牛
            HashMap<Integer,Integer> ref = new HashMap<>(nums.length,1);
            for (int i = 0; i < nums.length; i++) {
                if(ref.containsKey(nums[i])){
                    return new int[]{ref.get(nums[i]),i};
                }else{
                    ref.put(target-nums[i],i);
                }
            }
            return null;
        }
    }
}


