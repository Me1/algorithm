package com.sy.algorithm;

/**
 * 6. Z 字形变换
 *
 * 将一个给定字符串 s 根据给定的行数 numRows ，以从上往下、从左到右进行 Z 字形排列。
 *
 * 比如输入字符串为 "PAYPALISHIRING" 行数为 3 时，排列如下：
 *
 * P   A   H   N
 * A P L S I I G
 * Y   I   R
 * 之后，你的输出需要从左往右逐行读取，产生出一个新的字符串，比如："PAHNAPLSIIGYIR"。
 *
 * 请你实现这个将字符串进行指定行数变换的函数：
 *
 * string convert(string s, int numRows);
 *  
 *
 * 示例 1：
 *
 * 输入：s = "PAYPALISHIRING", numRows = 3
 * 输出："PAHNAPLSIIGYIR"
 * 示例 2：
 * 输入：s = "PAYPALISHIRING", numRows = 4
 * 输出："PINALSIGYAHRPI"
 * 解释：
 * P     I    N
 * A   L S  I G
 * Y A   H R
 * P     I
 * 示例 3：
 *
 * 输入：s = "A", numRows = 1
 * 输出："A"
 *  
 *
 * 提示：
 *
 * 1 <= s.length <= 1000
 * s 由英文字母（小写和大写）、',' 和 '.' 组成
 * 1 <= numRows <= 1000
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/zigzag-conversion
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * @Author zb
 * @Date 2022/3/12 19:42
 */
public class Algorithm006 {

    public static void main(String[] args){

        Solution solution = new Solution();
        String s = "A";
        int numRows = 2;

        String result = solution.convert(s,numRows);

        System.out.println(result);


    }

    private static class Solution {
        /**
         * 解提思路（一行就直接输出）
         *
         * 解题思路（至少两行） 经过找规律发现 每一行输出的都是 第一个字母 + 两个数 。只不过第一行 和最后这一行这两个数是一样的，一样的就不输出
         *
         * 假设行数是n，n1代表第一行 n2第二行 ni代表第i行
         *
         * 那么每一行输出的就是  ni、 上一个数+(2*n - i) 上一个数 + 2*(i - 1)、  上一个数+(2*n - i) 上一个数 + 2*(i - 1) 、 上一个数+(2*n - i) 上一个数 + 2*(i - 1)
         *
         * 很有规律
         *
         * @param s
         * @param numRows
         * @return
         */
        public String convert(String s, int numRows) {
            if(numRows == 1){
                return s;
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 1; i <= numRows; i++){
                int curIndex = i - 1;
                if(curIndex >= s.length()) {
                    continue;
                }
                char c = s.charAt(curIndex);
//                System.out.print(c);
                sb.append(c);
                while(curIndex < s.length()){
                    int diff1 = 2*(numRows - i);
                    curIndex += diff1;
                    if(diff1 > 0 && curIndex < s.length()){
                        c = s.charAt(curIndex);
//                        System.out.print(c);
                        sb.append(c);
                    }

                    //输出第二个数
                    int diff2 = 2*(i - 1);
                    curIndex += diff2;
                    if(diff2 > 0 && curIndex < s.length()){
                        c = s.charAt(curIndex);
//                        System.out.print(c);
                        sb.append(c);
                    }
                }
            }
            return sb.toString();
        }
    }
}
