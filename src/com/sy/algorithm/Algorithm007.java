package com.sy.algorithm;

/**
 * 7. 整数反转
 * 
 * 给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。
 *
 * 如果反转后整数超过 32 位的有符号整数的范围 [−231,  231 − 1] ，就返回 0。
 *
 * 假设环境不允许存储 64 位整数（有符号或无符号）。
 *  
 *
 * 示例 1：
 *
 * 输入：x = 123
 * 输出：321
 * 示例 2：
 *
 * 输入：x = -123
 * 输出：-321
 * 示例 3：
 *
 * 输入：x = 120
 * 输出：21
 * 示例 4：
 *
 * 输入：x = 0
 * 输出：0
 *  
 *
 * 提示：
 *
 * -2^31 <= x <= 2^31 - 1
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/reverse-integer
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * 
 * @Author zb
 * @Date 2022/3/14 18:29
 */
public class Algorithm007 {

    public static void main(String[] args){

        Solution solution = new Solution();
        int x = 1534236469;

        int result = solution.reverse(x);
        System.out.println(result);
    }

    private static class Solution {
        public int reverse(int x) {
            return execute2(x);
        }

        /**
         * 利用String的特点反转，用Long接收、要是不在int的范围返回0
         */
        public int execute1(int x){
            String ss = String.valueOf(x);
            StringBuilder resultStr = new StringBuilder();
            int startIndex = 0;
            if(ss.charAt(0) == '-'){
                resultStr.append('-');
                startIndex = 1;
            }
            for (int i = ss.length() - 1; i >= startIndex; i--) {
                resultStr.append(ss.charAt(i));
            }
            Long result = Long.parseLong(resultStr.toString());
            return result > Integer.MAX_VALUE || result < Integer.MIN_VALUE ? 0 : result.intValue();
        }


        /**
         * 位数循环，每次循环取当前的值*10 + %10
         */
        public int execute2(int x){
            int result = 0;
            while(x!=0){
                if( result < Integer.MIN_VALUE / 10 || result > Integer.MAX_VALUE / 10 ){
                    result = 0;
                    break;
                }
                int digit = x % 10;
                x /= 10;
                result = result * 10 + digit;
            }
            return result;
        }
    }
}
