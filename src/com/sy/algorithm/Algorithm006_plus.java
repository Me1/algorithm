package com.sy.algorithm;

/**
 * 6. Z 字形变换
 *
 * 将一个给定字符串 s 根据给定的行数 numRows ，以从上往下、从左到右进行 Z 字形排列。
 *
 * 比如输入字符串为 "PAYPALISHIRING" 行数为 3 时，排列如下：
 *
 * P   A   H   N
 * A P L S I I G
 * Y   I   R
 * 之后，你的输出需要从左往右逐行读取，产生出一个新的字符串，比如："PAHNAPLSIIGYIR"。
 *
 * 请你实现这个将字符串进行指定行数变换的函数：
 *
 * string convert(string s, int numRows);
 *  
 *
 * 示例 1：
 *
 * 输入：s = "PAYPALISHIRING", numRows = 3
 * 输出："PAHNAPLSIIGYIR"
 * 示例 2：
 * 输入：s = "PAYPALISHIRING", numRows = 4
 * 输出："PINALSIGYAHRPI"
 * 解释：
 * P     I    N
 * A   L S  I G
 * Y A   H R
 * P     I
 * 示例 3：
 *
 * 输入：s = "A", numRows = 1
 * 输出："A"
 *  
 *
 * 提示：
 *
 * 1 <= s.length <= 1000
 * s 由英文字母（小写和大写）、',' 和 '.' 组成
 * 1 <= numRows <= 1000
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/zigzag-conversion
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * @Author zb
 * @Date 2022/3/12 19:42
 */
public class Algorithm006_plus {

    public static void main(String[] args){

        Solution solution = new Solution();
        String s = "PAYPALISHIRING";
        int numRows = 3;

        String result = solution.convert(s,numRows);

        System.out.println(result);


    }

    private static class Solution {

        /**
         * 方法一：可以采取二维矩阵的方式求解，即先按顺序Z把二维矩阵填充，再遍历二维矩阵，只要不是0的就输出
         *
         * 方法二：压缩矩阵空间的方式求，因为方法一中很多0是很占空间的，所以可以使用压缩矩阵
         * 注意到每次往矩阵的某一行添加字符时，都会添加到该行上一个字符的右侧，且最后组成答案时只会用到每行的非空字符。
         * 因此我们可以将矩阵的每行初始化为一个空列表，每次向某一行添加字符时，添加到该行的列表末尾即可。
         *
         */
        public String convert(String s, int numRows) {

            if(numRows == 1 || numRows >= s.length()){
                return s;
            }

            StringBuffer[] mat = new StringBuffer[numRows];
            for (int i = 0; i < numRows; ++i) {
                mat[i] = new StringBuffer();
            }

            for(int i = 0,x = 0,t = numRows * 2 - 2;i < s.length(); i++){
                mat[x].append(s.charAt(i));
                if (i % t < numRows - 1) {
                    ++x;
                } else {
                    --x;
                }
            }
            StringBuffer ans = new StringBuffer();
            for (StringBuffer row : mat) {
                ans.append(row);
            }
            return ans.toString();
        }
    }
}
