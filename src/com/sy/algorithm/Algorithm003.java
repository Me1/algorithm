package com.sy.algorithm;

import java.util.HashMap;

/**
 * 
 * 给定一个字符串 s ，请你找出其中不含有重复字符的  最长子串  的长度。
 *
 *   
 *
 * 示例  1:
 *
 * 输入: s = "abcabcbb"
 * 输出: 3 
 * 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
 * 示例 2:
 *
 * 输入: s = "bbbbb"
 * 输出: 1
 * 解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
 * 示例 3:
 *
 * 输入: s = "pwwkew"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是  "wke"，所以其长度为 3。
 *       请注意，你的答案必须是 子串 的长度，"pwke"  是一个子序列，不是子串。
 *   
 *
 * 提示：
 *
 * 0 <= s.length <= 5 * 10^4
 * s  由英文字母、数字、符号和空格组成
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/longest-substring-without-repeating-characters
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * 
 * @Author zb
 * @Date 2022/3/9 10:12
 */
public class Algorithm003 {
    public static void main(String[] args){

        Solution solution = new Solution();
        String s = "a";
        int result = solution.lengthOfLongestSubstring(s);
        System.out.println(result);
    }

    private static class Solution {
        public int lengthOfLongestSubstring(String s) {
            int maxLength = 0;
            for (int i = 0; i < s.length() && maxLength < s.length() - i; i++) {
                String currentStr = s.charAt(i)+"";
                for (int j = i+1; j < s.length(); j++) {
                    String nextChar = s.charAt(j)+"";
                    if(currentStr.indexOf(nextChar)>=0){
                        break;
                    }
                    currentStr = s.substring(i,j+1);
                }
                if(currentStr.length()>maxLength){
                    maxLength = currentStr.length();
                }
            }
            return maxLength;
        }
    }
}
