package com.sy.algorithm;

/**
 *
 * 4. 寻找两个正序数组的中位数
 * 
 * 给定两个大小分别为 m 和 n 的正序（从小到大）数组 nums1 和 nums2。请你找出并返回这两个正序数组的 中位数 。
 *
 * 算法的时间复杂度应该为 O(log (m+n)) 。
 *
 *  
 *
 * 示例 1：
 *
 * 输入：nums1 = [1,3], nums2 = [2]
 * 输出：2.00000
 * 解释：合并数组 = [1,2,3] ，中位数 2
 * 示例 2：
 *
 * 输入：nums1 = [1,2], nums2 = [3,4]
 * 输出：2.50000
 * 解释：合并数组 = [1,2,3,4] ，中位数 (2 + 3) / 2 = 2.5
 *  
 *
 *  
 *
 * 提示：
 *
 * nums1.length == m
 * nums2.length == n
 * 0 <= m <= 1000
 * 0 <= n <= 1000
 * 1 <= m + n <= 2000
 * -10^6 <= nums1[i], nums2[i] <= 10^6
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/median-of-two-sorted-arrays
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * @Author zb
 * @Date 2022/3/10 18:57
 */
public class Algorithm004 {

    public static void main(String[] args){

        Solution solution = new Solution();

        int[] nums1 = new int[]{1};
        int[] nums2 = new int[]{};

        double result = solution.findMedianSortedArrays(nums1,nums2);

        System.out.println(result);
    }


    private static class Solution {
        public double findMedianSortedArrays(int[] nums1, int[] nums2) {

            int[] collection = new int[nums1.length+nums2.length];

            int collectIndex = 0;
            int indexNums2 = 0;
            for (int i = 0; i < nums1.length; i++) {
                    if(indexNums2 < nums2.length && nums1[i] > nums2[indexNums2]){
                        collection[collectIndex] = nums2[indexNums2];
                        collectIndex++;
                        indexNums2++;
                        i--;
                    }else{
                        collection[collectIndex] = nums1[i];
                        collectIndex++;
                    }
            }
            if(indexNums2 < nums2.length){
                for ( int i = indexNums2; i < nums2.length; i++) {
                    collection[collectIndex] = nums2[i];
                    collectIndex++;
                }
            }

            return collection.length % 2 == 0? (collection[collection.length/2 - 1] + collection[collection.length/2])/2.0 : collection[collection.length/2];
        }
    }
}
